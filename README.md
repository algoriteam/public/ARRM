Projeto em Engenharia Informática

Título:

    ARRM Framework - Assistência Remota com Realidade Mista + Scene Understanding + Communications + Data representation + Hololens

Autores:

    A70377 André Filipe Proença e Silva
    A70719 Diogo Ramos Constâncio
    A71184 João Pedro Fontes
    A59776 Marcos Filipe Andrade
    A71664 Nelson Figueiredo Silva
    A64309 Pedro Vieira Fortes
    A70443 Tiago Lopes Carvalhais


Contato Email:
{a70377,a70719,a71184,a59776,a71664,a64309,a70443}@alunos.uminho.pt